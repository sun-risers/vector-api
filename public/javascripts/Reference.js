//Creating middleware that requires a login for certain pages.
function requiresLogin(req, res, next) {
  if (req.session && req.session.userId) {
    return next();
  } else {
    var err = new Error('You must be logged in to view this page.');
    err.status = 401;
    return next(err);
  }
}
// router.get('/profile', mid.requiresLogin, function(req, res, next) {
//   //...
// });



//For inserting multiple questions and answers at a time
answers.collection.insert(entityData,function(err, result) {
 // answers is the data collection
});