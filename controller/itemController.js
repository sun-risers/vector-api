var mongoose = require('mongoose')
var entitySchema = require('../models/entity')
var db = require('../db') // For getting db state
var entities = mongoose.model("item", entitySchema)
module.exports = {
	findAll: function(req,res) {
		var params = {};
		console.log("inside get",req.query)
		if (req.query) {
			params = req.query
		}
		entities.find(params, function(err, result) {
		    if (err) throw err;
			res.send({"result":result})
		  });
	},
	findOne: function(req,res) {
		var params = {};
		if (req.query) {
			params = req.query
		}
		entities.findOne(params, function(err, result) {
		    if (err) throw err;
		    if (!result) {
		    	res.send({"status":"No entity found	"})
		    }
			res.send({"result":result})
		  });
	},
	create: function(req,res) {
		if (req.body) {
			var entityData = req.body;

			entityData.type = req.params.type;
			delete entityData.__v;
			req.body = entityData;
		}
		entities.create(req.body,function(err, result) {
			if (err) {
				res.status(500)
				console.log("Error",err)
  				// return res.render(JSON.stringify(err)) ---- for error stack
  				return res.json({"status":"Invalid data"})
			}
			var data = { "status" : "Success"};
			data.rows = result;
			res.json(data);
		})
	},
	update: function(req,res) {
		var updatedEntity = {new : true};
		entities.findByIdAndUpdate(req.params.entityID,req.body,updatedEntity,function(err, result) {
			if (err) {
				res.status(500)
				console.log("Error",err)
  				return res.json({"status":"Invalid data"})
			}
			var data = { "status" : "Success"};
			data.rows = result;
			res.json(data);
		})
	},
	delete: function(req,res) {

		entities.findByIdAndRemove(req.params.entityID,function(err, result) {
			if (err) {
				res.status(500)
				console.log("Error",err)
  				return res.json({"status":"Invalid data"})
			}
			console.log("inside del result",result)
			var data = { "status" : "Success"};
			res.json(data);
		})
	}
}