var mongoose = require('mongoose');
var configSchema = require('../models/config')

var config = mongoose.model('config', configSchema);

configController = {
	getRoles: function(req,res) {
		config.find({type: "role"}, function(err, result) {
		    if (err) throw err;
			res.send({"result":result})
		  });
	}
}

module.exports = configController