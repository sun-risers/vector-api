var mongoose = require('mongoose')
var userSchema = require('../models/users')
var db = require('../db') // For getting db state
var users = mongoose.model("users", userSchema)
module.exports = {
	get: function(req,res) {
		console.log("inside user get req",req.session)
		users.find({}, function(err, result) {
		    if (err) throw err;
			res.send({"result":result})
		  });
	},

	create: function(req,res) {
		users.create(req.body,function(err, result) {
			if (err) {
				res.status(500)
				console.log("Error",err)
  				// return res.render(JSON.stringify(err)) ---- for error stack
  				return res.json({"status":"Invalid data or data already exists"})
			}
			var data = { "status" : "Success"};
			data.rows = result;
			res.json(data);
		})
	},
	update: function(req,res) {
		var updatedUser = {new : true};
		users.findByIdAndUpdate(req.params.userId,req.body,updatedUser,function(err, result) {
			if (err) {
				res.status(500)
				console.log("Error",err)
  				return res.json({"status":"Invalid data or data already exists"})
			}
			var data = { "status" : "Success"};
			data.rows = result;
			res.json(data);
		})
	},
	delete: function(req,res) {

		users.findByIdAndRemove(req.params.userId,function(err, result) {
			if (err) {
				res.status(500)
				console.log("Error",err)
  				return res.json({"status":"Invalid data"})
			}
			var data = { "status" : "Success"};
			res.json(data);
		})
	}
}