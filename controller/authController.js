var express = require('express');
var mongoose = require('mongoose');
var userSchema = require('../models/users')
var bcrypt = require('bcrypt')
var jwt = require('jsonwebtoken')
var config = require('../config')
var app = express();
var users = mongoose.model('user', userSchema);
encryptPassword();
authController = {
	signin: function(req,res,next) {
			var query = req.query;
			var session = req.session;
		// Down comment method is passport.js authentication
		// userSchema.statics.authenticate = function (email, password, callback) {//doubt with input email and password
			users.findOne({"loginName":query.loginName,"roleCode":query.roleCode}) // send the roleCode for differentiating user
				.exec(function (err, user) {
					if (err) {
						return res.send(err)
					} else if (!user) {
						return res.sendStatus(401)
					}	

					//Compare the text password from user login with saved hash password
					bcrypt.compare(query.password, user.password, function (err, result) {
					if (result) {
						// session.userId = user._id;
						// session.loginRetried = 5;
					    const payload = {
					    	exp: Math.floor(Date.now() / 1000) + (60 * 60),
					      loginName: user.loginName
					    };
					    // sign with default (HMAC SHA256)
				        var token = jwt.sign(payload, config.secret); // or {expiresIn : "1h"}
				        var data = {
				        	status : "Success",
				        	token : token
				        }

						return res.json(data)
					}
					return res.sendStatus(401)
					})
				});
		// }
		
	},
	signout:function(req,res,next) {
		console.log("inside logout",req.session)
		if (req.session) {
		// delete session object
		req.session.destroy(function(err) {
			if(err) {
			return next(err);
			} else {
			return res.send({"status":"Success","token": null});
			}
		});
		}
	}
}

function encryptPassword() {

	userSchema.pre('save', function (next) {
		var user = this;
		bcrypt.hash(user.password, 10, function (err, hash){
		  if (err) {
			return next(err);
		  }
		  user.password = hash;
		  next();
		})
	  });
}

module.exports = authController