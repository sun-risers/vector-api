var mongoose = require('mongoose');
var fs = require('fs');
var pdf = require('dynamic-html-pdf');
var users = mongoose.model("users", userSchema)

pdfController = {
	getPdfData: function(req,res) {
		var params = req.params;
		users.findOne(params, function(err, result) {
		    if (err) throw err;

			var html = fs.readFileSync('./templates/userForm.html', 'utf8');
			// var options = { format: 'Letter' };

			var options = {
			    format: "A3",
			    orientation: "portrait",
			    border: "10mm"
			};

			var userData = JSON.parse(result);  // data should provide to bind the data in HTML

			var document = {
			    type: 'buffer',     // 'file' or 'buffer'
			    template: html,
			    context: {
			        userData: userData
			    }
			};

			pdf.create(document, options)
			    .then(buffer => {
					res.header('content-type', 'application/pdf');
					res.send(buffer);
			    })
			    .catch(error => {
			        console.error(error)
			    });	  
			});			 
		}
	}
}

module.exports = pdfController