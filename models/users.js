var mongoose = require('mongoose')
var userSchema = mongoose.Schema({
	firstName: String,
	lastName: String,
    phone:Number,
    loginName: {    type: String,
		    required: true,
		    trim: true
		},
    email: {
    	    type: String,
		    required: true,
		    trim: true
	    },
    password:{
	        type: String,
		    required: true
		},
    roleCode:{
	        type: String,
		    required: true
		}
  });
userSchema.index({ "loginName": 1,"roleCode":1}, { "unique": true });
module.exports = userSchema;


