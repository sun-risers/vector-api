var mongoose = require('mongoose')
var entitySchema = mongoose.Schema({
    type: {    
    		type: String,
		    required: true,
		    trim: true
		}
  },{ strict: false , versionKey: false});

module.exports = entitySchema;


