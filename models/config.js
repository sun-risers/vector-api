var mongoose = require('mongoose')
var configSchema = mongoose.Schema({
    name: {    
    		type: String,
		    required: true,
		    trim: true
		},
    roleCode:{
	        type: String,
		    required: true
		}
  });

module.exports = {configSchema};


