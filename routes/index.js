var express = require('express');
var jwt = require('jsonwebtoken')
var config = require('../config')
var router = express.Router();
const helmet = require('helmet')// need to know


// importing controller folders

var baseController = require('../controller/baseController')
var userController = require('../controller/userController')
var authController = require('../controller/authController')
var itemController = require('../controller/itemController')
var configController = require('../controller/configController')
var pdfController = require('../controller/commons/pdfController')




// Authentication routing
router.get('/login',authController.signin)
router.post('/logout',authController.signout)

// a middleware function with no mount path. This code is executed for every request to the router
router.use(function (req, res, callCurrentRouteMethod) {
  console.log('Time:', Date.now())
  routeMiddleExec(req, res,callCurrentRouteMethod);
  // callCurrentRouteMethod()
})

/* GET home page and basic */

router.get('/',baseController.home)

// Getting roles

router.get('/config/roles/list',configController.getRoles)

// Basic entity crud operations

router.get('/entity/:type/list',itemController.findAll)
router.get('/entity/:type/entity',itemController.findOne)
router.post('/entity/:type/create',itemController.create)
router.put('/entity/:type/:entityID',itemController.update)
router.delete('/entity/:type/:entityID',itemController.delete)


// Users routing

router.get('/auth/users',userController.get)
router.post('/auth/createUser',userController.create) // Act as signup
router.put('/auth/updateUser/:userId',userController.update)
router.delete('/auth/deleteUser/:userId',userController.delete)


// Common routing

router.get('/user/pdfData',pdfController.getPdfData) // Getting PDF data

module.exports = router;

function routeMiddleExec(req, res,next) {

  // check header or url parameters or post parameters for token
  var token = req.headers['x-access-token'];

  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, config.secret, function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;    
        next();
      }
    });

  } else {
    // if there is no token
    // return an error
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });

  }
}
